<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Allio\ChangeRequests\Models\{ChangeRequest, ChangeRequestItem};

class ChangeRequestsAttachments extends Migration
{
    /** @var string */
    private $_requests_table;
    
    /** @var string */
    private $_attachments_table;
    
    public function __construct() {
        $this->_requests_table = config('change-requests.tables.requests');
        $this->_attachments_table = config('change-requests.tables.attachments', 'tbl_change_request_attachments');
    }
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->_attachments_table, function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('change_request_id')->index('change_request_id')->comment('ID ziadosti');
            $table->string('file', 500)->comment('Nazov suboru');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            
            $table->foreign('change_request_id')->references('id')->on($this->_requests_table)->onDelete('cascade');
        });
        
        // add table comments
        \DB::statement("ALTER TABLE `".\DB::getTablePrefix().$this->_attachments_table."` comment 'Prilohy hlaseni zmien'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists($this->_attachments_table);
    }
}