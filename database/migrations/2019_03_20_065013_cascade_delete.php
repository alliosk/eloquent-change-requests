<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Allio\ChangeRequests\Models\{ChangeRequest, ChangeRequestItem};

class CascadeDelete extends Migration
{
    /** @var string */
    private $_requests_table;
    
    /** @var string */
    private $_items_table;
    
    public function __construct() {
        $this->_requests_table = config('change-requests.tables.requests');
        $this->_items_table = config('change-requests.tables.items');
    }
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table($this->_items_table, function (Blueprint $table) {
            $table->dropForeign(['change_request_id']);
            $table->foreign('change_request_id')->references('id')->on($this->_requests_table)->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}