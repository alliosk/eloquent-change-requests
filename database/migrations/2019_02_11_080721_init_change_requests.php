<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Allio\ChangeRequests\Models\{ChangeRequest, ChangeRequestItem};

class InitChangeRequests extends Migration
{
    /** @var string */
    private $_requests_table;
    
    /** @var string */
    private $_items_table;
    
    public function __construct() {
        $this->_requests_table = config('change-requests.tables.requests');
        $this->_items_table = config('change-requests.tables.items');
    }
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->_requests_table, function (Blueprint $table) {
            $table->increments('id');
            
            foreach(config('change-requests.foreign_keys', []) as $attribute => $class){
                $model = new $class();
                $table->unsignedInteger($attribute)->nullable()->index($attribute);
                $table->foreign($attribute, "fk_$attribute")->references($model->getKeyName())->on($model->getTable());
            }
            
            $table->enum('action', ['create','update','delete'])->default('update')->comment('Typ pozadovanej akcie vykonanej na objekte');
            $table->string('table_name', 250)->nullable()->comment('Nazov tabulky');
            $table->unsignedInteger('table_pk')->index('table_pk')->nullable()->comment('PK v tabulke');
            $table->text('note')->nullable()->comment('Poznamka administratora');
            $table->tinyInteger('state')->index('state')->default(ChangeRequest::STATE_ACTIVE)->comment('Stav');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
        
        Schema::create($this->_items_table, function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('change_request_id')->index('change_request_id')->comment('ID ziadosti');
            $table->string('relation_table', 250)->nullable()->comment('Nazov relacnej tabulky v pripade potreby');
            $table->string('field', 250)->comment('zmenene pole v relacii');
            $table->text('new_value')->nullable()->comment('nova hodnota pola');
            $table->text('old_value')->nullable()->comment('stara hodnota pola');
            $table->text('new_value_display')->nullable()->comment('Nova hodnota pre zobrazenie');
            $table->text('old_value_display')->nullable()->comment('Stara hodnota pre zobrazenie');
            $table->unsignedInteger('user_id')->index('user_id')->nullable()->comment('User ID');
            $table->tinyInteger('state')->index('state')->default(ChangeRequest::STATE_ACTIVE)->comment('Stav');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            
            $table->foreign('change_request_id')->references('id')->on($this->_requests_table);
        });
        
        // add table comments
        \DB::statement("ALTER TABLE `".\DB::getTablePrefix().$this->_requests_table."` comment 'Ziadosti o hlasenia zmien'");
        \DB::statement("ALTER TABLE `".\DB::getTablePrefix().$this->_items_table."` comment 'Konkretne hlasenia zmien na atributoch'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->_items_table);
        Schema::dropIfExists($this->_requests_table);
    }
}