<?php

namespace Allio\ChangeRequests\Formatters;

use Allio\ChangeRequests\ChangeRequestContract;

abstract class ChangeRequestItemFormatter
{
    protected $model;
    protected $attribute;
    
    public function __construct(ChangeRequestContract $model, $attribute){
        $this->model = $model;
        $this->attribute = $attribute;
    }
    
    abstract public function newValue() : ?string;
    abstract public function newValueDisplay() : ?string;
    abstract public function oldValue() : ?string;
    abstract public function oldValueDisplay() : ?string;
}
