<?php

namespace Allio\ChangeRequests\Formatters;

class ToDateFormatter extends ChangeRequestItemFormatter
{
    public function newValue() : ?string {
        $value = $this->model->{$this->attribute};
        return $value === NULL ? NULL : date('Y-m-d', strtotime($value));
    }
    
    public function newValueDisplay() : ?string {
        $value = $this->model->{$this->attribute};
        return $value === NULL ? NULL : date('j.n.Y', strtotime($value));
    }
    
    public function oldValue() : ?string {
        $value = $this->model->getOriginal($this->attribute);
        return $value === NULL ? NULL : date('Y-m-d', strtotime($value));
    }
    
    public function oldValueDisplay() : ?string {
        $value = $this->model->getOriginal($this->attribute);
        return $value === NULL ? NULL : date('j.n.Y', strtotime($value));
    }
}
