<?php

namespace Allio\ChangeRequests;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Illuminate\Contracts\Foundation\Application;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * Bootstrap the service provider.
     *
     * @return void
     */
    public function boot()
    {
        $this->setupConfig($this->app);
        //var_dump("HERE");exit;
        $this->loadMigrationsFrom( __DIR__ . '/../database/migrations');
    }
    
    /**
     * Setup the config.
     *
     * @param $app
     *
     * @return void
     */
    protected function setupConfig(Application $app)
    {
        $config = realpath(__DIR__.'/../config/change-requests.php');

        if ($app->runningInConsole()) {
            $this->publishes([
                $config => base_path('config/change-requests.php'),
            ]);
        }

        $this->mergeConfigFrom($config, 'change-requests');
    }
    
}