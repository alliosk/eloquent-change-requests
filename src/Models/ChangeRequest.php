<?php
namespace Allio\ChangeRequests\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

class ChangeRequest extends Model
{
    const STATE_DECLINED = 0;
    const STATE_ACTIVE = 1;
    const STATE_APPROVED = 2;
    
    const ACTION_CREATE = 'create';
    const ACTION_UPDATE = 'update';
    const ACTION_DELETE = 'delete';
    
    /** @var array - pole dynamickych cudzich klucov pre relaciu owner */
    protected $foreign_keys = [];
    
    /** {@inheritdoc} */
    protected $attributes = [
        'state' => self::STATE_ACTIVE,
    ];
    
    /** {@inheritdoc} */
    protected $fillable = [
        'action', 'table_name', 'table_pk', 'note', 'state'
    ];
    
    /** {@inheritdoc} */
    public function __construct(array $attributes = array()) {
        $this->table = config('change-requests.tables.requests', NULL);
        $this->foreign_keys = config('change-requests.foreign_keys', []);
        
        foreach($this->foreign_keys as $attribute => $class){
            $this->fillable[] = $attribute;
        }
        
        parent::__construct($attributes);
    }
    
    /** @return \Illuminate\Database\Eloquent\Relations\BelongsTo */
    public function owner() {
        foreach($this->foreign_keys as $key => $class){
            if($this->$key !== NULL){
                return $this->belongsTo($class, $key);
            }
        }
        throw new \Allio\ChangeRequests\Exceptions\OwnerNotSetException();
    }
    
    /** @return \Illuminate\Database\Eloquent\Relations\HasMany */
    public function changeRequestItems() : HasMany {
        return $this->hasMany(ChangeRequestItem::class, 'change_request_id');
    }
    
    /** @return \Illuminate\Database\Eloquent\Relations\HasMany */
    public function attachments() : HasMany {
        return $this->hasMany(ChangeRequestAttachment::class, 'change_request_id');
    }
    
    /**
     * @return Collection
     */
    public function getItemsByFieldAttribute() : Collection {
        return $this->changeRequestItems->keyBy('field');
    }
    
    /**
     * vrati hodnotu zmeny pre dany atribut
     * @param string $attribute
     * @return string|NULL
     */
    public function getChangeFor($attribute) {
        $items = $this->items_by_field;
        return $items->has($attribute) ? $items[$attribute]->new_value_display : false;
    }
}