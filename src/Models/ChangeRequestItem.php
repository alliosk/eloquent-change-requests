<?php
namespace Allio\ChangeRequests\Models;

use \Illuminate\Database\Eloquent\Relations\BelongsTo;
use \Illuminate\Database\Eloquent\Model;

class ChangeRequestItem extends Model
{
    /** {@inheritdoc} */
    protected $fillable = [
        'change_request_id', 'relation_table', 'field', 'new_value',
        'old_value', 'new_value_display', 'old_value_display', 'state'
    ];
    
    /** {@inheritdoc} */
    public function __construct(array $attributes = array()) {
        $this->table = config('change-requests.tables.items');
        
        parent::__construct($attributes);
    }
    
    /** @return \Illuminate\Database\Eloquent\Relations\BelongsTo */
    public function changeRequest() : BelongsTo
    {
        return $this->belongsTo(ChangeRequest::class, 'change_request_id');
    }
    
    /**
     * Scope - scope pre iba aktivne záznamy
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('state', ChangeRequest::STATE_ACTIVE);
    }
}