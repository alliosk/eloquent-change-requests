<?php
namespace Allio\ChangeRequests\Models;

use \Illuminate\Database\Eloquent\Relations\BelongsTo;
use \Illuminate\Database\Eloquent\Model;

class ChangeRequestAttachment extends Model
{
    /** {@inheritdoc} */
    protected $fillable = [
        'change_request_id', 'file'
    ];
    
    /** {@inheritdoc} */
    public function __construct(array $attributes = array()) {
        $this->table = config('change-requests.tables.attachments');
        
        parent::__construct($attributes);
    }
    
    /** @return \Illuminate\Database\Eloquent\Relations\BelongsTo */
    public function changeRequest() : BelongsTo {
        return $this->belongsTo(ChangeRequest::class, 'change_request_id');
    }
}