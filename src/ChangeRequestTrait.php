<?php

namespace Allio\ChangeRequests;

use \Allio\ChangeRequests\Formatters\ChangeRequestItemFormatter;
use \Allio\ChangeRequests\Models\{ChangeRequest, ChangeRequestAttachment, ChangeRequestItem};
use \Illuminate\Support\{Arr, Collection, Str};

trait ChangeRequestTrait 
{
    private $_active_change_request;
    
    private $_result = false;
    
    private $_change_request_enabled = true;
    
    private $_files = [];
    
    /**
     * @return mixed - attachments configuration
     */
    public function attachments() : Collection {
        return new Collection([]);
    }
    
    /**
     * @return bool - je zber udajov povoleny? (napr pre prihlaseneho usera, danu entitu a pod)
     */
    public function changeRequestEnabled() : bool
    {
        return $this->_change_request_enabled;
    }
    
    /** disable change requests functionality */
    public function disableChangeRequests()
    {
        $this->_change_request_enabled = false;
    }
    
    /** enable change requests functionality */
    public function enableChangeRequests()
    {
        $this->_change_request_enabled = true;
    }
    
    /**
     * ulozi ziadost o vyvtorenie daneho modelu
     * @return bool - bolo ukladanie zmien uspesne?
     */
    public function saveCreateChanges(array $files = []) : bool
    {
        $this->_files = $files;
        return $this->saveChanges(ChangeRequest::ACTION_CREATE);
    }
    
    /**
     * ulozi ziadost o aktualizaciu daneho modelu
     * @return bool - bolo ukladanie zmien uspesne?
     */
    public function saveUpdateChanges(array $files = []) : bool
    {
        $this->_files = $files;
        return $this->saveChanges(ChangeRequest::ACTION_UPDATE);
    }
    
    /**
     * ulozi ziadost o zmazanie daneho modelu
     * @return bool - bolo ukladanie zmien uspesne?
     */
    public function saveDeleteChanges() : bool
    {
        return $this->saveChanges(ChangeRequest::ACTION_DELETE);
    }
    
    /**
     * @return bool - bolo ukladanie zmien uspesne?
     */
    public function changesSaved() : bool
    {
        return $this->_result;
    }
    
    /**
     * @param string $attribute
     * @return NULL|string|bool
     */
    public function getChangeFor($attribute)
    {
        $request = $this->getActiveUpdateChangeRequest();
        if($request === NULL){
            return false;
        }
        return $request->getChangeFor($attribute);
    }
    
    /**
     * vrati aktivnu ziadost o aktualizaciu pre dany zaznam
     * @return ChangeRequest|NULL
     */
    public function getActiveUpdateChangeRequest() : ?ChangeRequest
    {
        return $this->getActiveChangeRequest(ChangeRequest::ACTION_UPDATE);
    }
     
    /**
     * vrati aktivnu ziadost o zmazanie pre dany zaznam
     * @return ChangeRequest|NULL
     */
    public function getActiveDeleteChangeRequest() : ?ChangeRequest
    {
        return $this->getActiveChangeRequest(ChangeRequest::ACTION_DELETE);
    }
    
    /**
     * vrati zoznam aktivnych ziadost o vytvorenie novych zaznamov
     * @return ChangeRequest[]
     */
    public function getActiveCreateChangeRequests($relation_class)
    {
        $request_attributes = $this->changeRequestForeigns()->merge([
            'action' => ChangeRequest::ACTION_CREATE,
            'table_name' => (new $relation_class())->getTable(),
            'state' => ChangeRequest::STATE_ACTIVE,
        ])->toArray();
        return ChangeRequest::with('changeRequestItems')->where($request_attributes)->get();
    }
    
    /**
     * vrati aktivnu ziadost o zmenu pre dany zaznam
     * @return ChangeRequest|NULL
     */
    protected function getActiveChangeRequest($action)
    {
        if(!isset($this->_active_change_request[$action])){
            $request_attributes = $this->changeRequestForeigns()->merge([
                'action' => $action,
                'table_name' => $this->getTable(),
                'table_pk' => $this->id,
                'state' => ChangeRequest::STATE_ACTIVE,
            ])->toArray();
            
            $this->_active_change_request[$action] = ChangeRequest::with('changeRequestItems')->where($request_attributes)->first();
        }
        return $this->_active_change_request[$action];
    }
    
    /**
     * @param string $operation - o aky typ operacie ide?
     * @return bool - bolo ukladanie zmien uspesne?
     */
    protected function saveChanges($operation) : bool
    {
        if(!$this->changeRequestEnabled()){
            return false;
        }
        
        $method = "_mark".Str::studly($operation)."Changes";
        if(!method_exists($this, $method)){
            return false;
        }
        
        if(!$this->isValid()){
            return false;
        }
        
        $this->_result = $this->$method();
        return $this->_result;
    }
    
    /**
     * @return bool - bol create request ulozeny?
     */
    protected function _markCreateChanges() : bool
    {
        $items = [];
        foreach ($this->attributes as $attribute => $value) {
            if ($data = $this->getAttributeChangeable($attribute)) {
                $items[$attribute] = new ChangeRequestItem($data);
            }
        }
        $request = $this->_buildNewChangeRequest(ChangeRequest::ACTION_CREATE);
        return $this->_saveChangeRequestInTransaction($request, $items);
    }
    
    /**
     * @return bool - bol update request ulozeny?
     */
    protected function _markUpdateChanges() : bool
    {
        $request = $this->getActiveChangeRequest(ChangeRequest::ACTION_UPDATE);
        if($request === NULL){
            $request = $this->_buildNewChangeRequest(ChangeRequest::ACTION_UPDATE);
        }
        
        $items = $request->changeRequestItems->keyBy('field')->all();
        foreach ($this->getDirty() as $attribute => $value) {
            if ($data = $this->getAttributeChangeable($attribute)) {
                if(isset($items[$attribute])){
                    $items[$attribute]->fill($data);
                }
                else{
                    $items[$attribute] = new ChangeRequestItem($data);
                } 
            }
        }
        
        return $this->_saveChangeRequestInTransaction($request, $items);
    }
    
    /**
     * @return bool - bol delete request ulozeny?
     */
    protected function _markDeleteChanges() : bool
    {
        $request = $this->_buildNewChangeRequest(ChangeRequest::ACTION_DELETE);
        return $request->save();
    }

    /**
     * Je ukladanie zmeny na atribute povolene?
     *
     * @param string $attribute
     *
     * @return mixed
     */
    protected function getAttributeChangeable($attribute)
    {
        $changeable = $this->changeRequestAttributes();
        if(!$changeable->has($attribute)){
            return false;
        } 
        
        $definition = $changeable[$attribute];
        $return = [
            'field' => $attribute,
            'state' => ChangeRequest::STATE_ACTIVE,
        ];
        if(is_object($definition) && ($definition instanceof ChangeRequestItemFormatter)){
            $return['new_value'] = $definition->newValue();
            $return['new_value_display'] = $definition->newValueDisplay();
            $return['old_value'] = $definition->oldValue();
            $return['old_value_display'] = $definition->oldValueDisplay();
        }
        else{
            $class = get_class($this);
            $new_model = new $class($this->original);
            $return['new_value'] = (string)Arr::get($this->attributes, $attribute);
            $return['new_value_display'] = $this->$definition;
            $return['old_value'] = (string)$this->getOriginal($attribute);
            $return['old_value_display'] = (string)$new_model->$definition;
        }
        return $return;
    }
    
    /**
     * @param string $action
     * @return ChangeRequest
     */
    protected function _buildNewChangeRequest($action) : ChangeRequest
    {
        return new ChangeRequest($this->changeRequestForeigns()->merge([
            'action' => $action,
            'table_name' => $this->getTable(),
            'table_pk' => $this->id,
            'state' => ChangeRequest::STATE_ACTIVE,
        ])->toArray());
    }
    
    /**
     * ulozi request v transakcii spolu s jeho items
     * @param ChangeRequest $request
     * @param Collection $items
     * @return bool
     * @throws \Exception
     */
    protected function _saveChangeRequestInTransaction(ChangeRequest $request, array $items) : bool
    {
        if(empty($items)){
            return true;
        }
        $connection = $this->getConnection();
        $new_transaction = $connection->transactionLevel() == 0;
        if($new_transaction){
            $connection->beginTransaction();
        }
        
        try {
            $request->save();
            $request->changeRequestItems()->saveMany($items);
            if(!empty($this->_files)){
                $attachments = [];
                foreach($this->_files as $file){
                    $disk = config('change-requests.disk');
                    $folder = config('change-requests.upload_folder');
                    if (!\Storage::disk($disk)->exists($folder)) {
                        \Storage::disk($disk)->makeDirectory($folder, 0775, true, true);
                    }
                    $path = $file->storeAs($folder, \Illuminate\Support\Str::random(40).".".$file->getClientOriginalExtension(), $disk);
                    $attachments[] = new ChangeRequestAttachment([
                        'change_request_id' => $request->id,
                        'file' => $path,
                    ]);
                }
                $request->attachments()->saveMany($attachments);
            }
            if($new_transaction){
                $connection->commit();
            }
            return true;
        } 
        catch(\Exception $e)
        {
            if($new_transaction)
                $connection->rollback();
            return false;
        }
    }
}
