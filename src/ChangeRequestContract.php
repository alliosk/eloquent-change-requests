<?php

namespace Allio\ChangeRequests;

use Illuminate\Support\Collection;
use Allio\ChangeRequests\Models\ChangeRequest;

interface ChangeRequestContract 
{
    /** 
     * @return array - pole nazvov atributov, ktorych zmeny sa maju zaznamenat 
     * format: [
     *  'person_id' => 'person_name' // kluc je nazov atributu, hodnota je nazov atributu s user-friendly hodnotou
     * ]
     */
    public function changeRequestAttributes() : Collection;
    
    /**
     * @return mixed - related model ID
     */
    public function changeRequestForeigns() : Collection;
    
    /**
     * @return mixed - attachments configuration
     */
    public function attachments() : Collection;
    
    /**
     * @return bool - je zber udajov povoleny? (napr pre prihlaseneho usera, danu entitu a pod)
     */
    public function changeRequestEnabled() : bool;
    
    /** disable change requests functionality */
    public function disableChangeRequests();
    
    /** enable change requests functionality */
    public function enableChangeRequests();
    
    /**
     * ulozi ziadost o vyvtorenie daneho modelu
     * @return bool - bolo ukladanie zmien uspesne?
     */
    public function saveCreateChanges(array $files = []) : bool;
    
    /**
     * ulozi ziadost o aktualizaciu daneho modelu
     * @return bool - bolo ukladanie zmien uspesne?
     */
    public function saveUpdateChanges(array $files = []) : bool;
    
    /**
     * ulozi ziadost o zmazanie daneho modelu
     * @return bool - bolo ukladanie zmien uspesne?
     */
    public function saveDeleteChanges() : bool;
    
    /**
     * @return bool - bolo ukladanie zmien uspesne?
     */
    public function changesSaved() : bool;
    
    /**
     * @param string $attribute
     * @return NULL|string
     */
    public function getChangeFor($attribute);
    
    /**
     * vrati aktivnu ziadost o aktualizaciu pre dany zaznam
     * @return ChangeRequest|NULL
     */
    public function getActiveUpdateChangeRequest() : ?ChangeRequest;
     
    /**
     * vrati aktivnu ziadost o zmazanie pre dany zaznam
     * @return ChangeRequest|NULL
     */
    public function getActiveDeleteChangeRequest() : ?ChangeRequest;
    
    /**
     * vrati zoznam aktivnych ziadost o vytvorenie novych zaznamov
     * @return ChangeRequest[]
     */
    public function getActiveCreateChangeRequests($relation_class);
    
}
