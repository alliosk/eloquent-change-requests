# eloquent-change-requests

Laravel knižnica pre hlásenie zmien údajov

## Konfigurácia

|Atribút        |Typ        | Popis                                                                             |
|---------------|-----------|-----------------------------------------------------------------------------------|
|tables         | array     | názvy tabuliek pre requesty a items                                               |
|foreign_keys   | array     | názvy stĺpcov/atribútov, ktoré sa doplnia k ChangeRequest modelu a slúžia na prepojenie s inými modelmi (napr. ["model_id" => \Namespace\To\Model::class]) |

## Inštalácia

1. Potrebné pridať ServiceProvider do config/app.php (ak je zakázaný automatický composer package discovery - https://laravel.com/docs/5.7/packages#package-discovery)

```
'providers' => [
    ...
    Allio\ChangeRequests\ServiceProvider::class,
    ...
],
```

2. vytvoriť kópiu konfiguračného súboru change-request.php do priečinku config a nastaviť potrebné názvy tabuliek resp cudzích kľúčov
3. spustiť migrácie