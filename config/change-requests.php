<?php

return [
    
    // nazvy databazovych tabuliek
    'tables' => [
        'requests' => 'change_requests',
        'items' => 'change_request_items',
        'attachments' => 'change_request_attachments',
    ],
    
    /** 
     * foreign keys 
     * ["model_id" => \Namespace\To\Model::class]
     */
    'foreign_keys' => [
        
    ],
    
    'disk' => 'local',
    
    'upload_folder' => 'change_requests/'.date("Y/m"),
    
];