<?php

namespace Allio\ChangeRequests\Tests\Unit;

//use Illuminate\Foundation\Testing\TestCase;
//use Tests\CreatesApplication;
use PHPUnit\Framework\TestCase;
use Illuminate\Routing\Route;
use Illuminate\Contracts\Config\Repository as Config;
use Illuminate\Http\Request;
use Illuminate\Container\Container;
use Mockery as m;
use Allio\ChangeRequests\Models\{ChangeRequest, ChangeRequestItem};

/**
 * Otestuje model Allio\ChangeRequests\Models\ChangeRequest
 */
class ChangeRequestTest extends TestCase
{
    private $_config_mock;
    
    public function setUp(){
        parent::setUp();
        $this->_config_mock = m::mock(Config::class);
        $this->_config_mock->shouldReceive('get')->with('change-requests.tables.requests', NULL)->andReturn(NULL);
        $this->_config_mock->shouldReceive('get')->with('change-requests.tables.items', NULL)->andReturn(NULL);
        $this->_config_mock->shouldReceive('get')->with('change-requests.foreign_keys', [])->andReturn([]);
        
        $app = new \Illuminate\Foundation\Application();
        Container::getInstance()->instance('config', $this->_config_mock);
        \Illuminate\Support\Facades\Facade::setFacadeApplication(Container::getInstance());
    }
    
    /** @covers ChangeRequest::owner */
    public function testOwner()
    {
        $owner1 = new Eloquent1();
        $owner1->id = 1;
        
        $request = new ChangeRequest(['foreign1_id'=>$owner1->id]);
        $this->expectException(\Allio\ChangeRequests\Exceptions\OwnerNotSetException::class);
        $request->owner;
    }
    
    /** @covers ChangeRequest::getItemsByFieldAttribute */
    public function testGetItemsByFieldAttribute()
    {
        $request = new ChangeRequest();
        $item1 = new ChangeRequestItem(['state' => ChangeRequest::STATE_ACTIVE, 'field'=>'field1', 'new_value_display' => 'Result1']);
        $item2 = new ChangeRequestItem(['state' => ChangeRequest::STATE_ACTIVE, 'field'=>'field2', 'new_value_display' => 'Result2']);
        $request->setRelation('changeRequestItems', collect([$item1, $item2]));
        
        $result = $request->items_by_field;
        $this->assertTrue($result->has('field1'));
        $this->assertTrue($result->has('field2'));
    }
    
    /** @covers ChangeRequest::getChangeFor */
    public function testGetChangeFor()
    {
        $request = new ChangeRequest();
        $item1 = new ChangeRequestItem(['state' => ChangeRequest::STATE_ACTIVE, 'field'=>'field1', 'new_value_display' => 'Result1']);
        $item2 = new ChangeRequestItem(['state' => ChangeRequest::STATE_ACTIVE, 'field'=>'field2', 'new_value_display' => 'Result2']);
        $request->setRelation('changeRequestItems', collect([$item1, $item2]));
        
        $this->assertEquals($request->getChangeFor('field1'), 'Result1');
        $this->assertEquals($request->getChangeFor('field2'), 'Result2');
    }
}

class Eloquent1 extends \Illuminate\Database\Eloquent\Model
{
    
}

class Eloquent2 extends \Illuminate\Database\Eloquent\Model
{
    
}